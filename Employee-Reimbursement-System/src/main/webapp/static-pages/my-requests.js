var employee;
const reimbursementRequestUrl = "http://localhost:8080/Employee-Reimbursement-System/requests";

$( document ).ready(function() {
    //document is ready
	/*
	//Hide login error alert by default
	$("#alertError").hide();
	
	//Let's add the click event for the button
	
	$( "#btnLogin" ).click(function() {
	  
	  //call the login method
	  login();
	  
	});*/
	
	let employeeStr = sessionStorage.getItem("employee")
	if(!employeeStr)
	{
		$(location).attr('href', 'login.html');
	}
	else
	{
		employee = JSON.parse(employeeStr);
		getReimbursmentRequests(employee.employeeId);
	}

});



function getReimbursmentRequests(employeeId) {
 	
	performAjaxGetRequest($`{reimbursementRequestUrl}?employeeId={employeeId}`, displayRequests);
}

function displayRequests(responseText) {
  //console.log("Success! You're logged in");
  console.log(responseText);
  let requests = JSON.parse(responseText);
  
}

function handleUnsuccessful() {
  console.log("Login unsuccessful");
  $("#alertError").show();
  $("#errorDetails").text("Username, role or password is invalid.");
}