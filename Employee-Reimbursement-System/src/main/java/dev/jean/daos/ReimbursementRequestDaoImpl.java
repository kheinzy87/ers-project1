package dev.jean.daos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dev.jean.enums.RequestStatus;
import dev.jean.models.EmployeeRequest;
import dev.jean.models.ReimbursementRequest;
import dev.jean.util.DbConnectionUtil;

public class ReimbursementRequestDaoImpl implements ReimbursementRequestDao{
	String sqlQuery;
	List<EmployeeRequest>requests;
	
	Logger log = LogManager.getLogger();

	public List<EmployeeRequest> getAllEmployeeRequests() throws SQLException{
		log.error("Getting the existing request from the database");
		
		sqlQuery="SELECT r.REIMBURSEMENT_ID, r.REQUEST_DATE, e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME, r.DESCRIPTION, r.AMOUNT," 
				+" rs.STATUS, r.PROCESSED_DATE, r.MANAGER_ID, e2.FIRST_NAME AS M_FIRST_NAME, e2.LAST_NAME AS M_LAST_NAME"
				+" FROM EMPLOYEE e " 
				+" INNER JOIN REIMBURSEMENT r ON e.EMPLOYEE_ID = r.EMPLOYEE_ID " 
				+" INNER JOIN REQUEST_STATUS rs ON r.STATUS_ID = rs.REQUEST_STATUS_ID" 
				+" LEFT OUTER JOIN EMPLOYEE e2 ON r.MANAGER_ID = e2.EMPLOYEE_ID";
		try(Connection dbConnection = DbConnectionUtil.getHardCodedConnection();
				Statement queryStatement = dbConnection.createStatement();){
				ResultSet resultSet= queryStatement.executeQuery(sqlQuery);
				
				return retrieveRequest(resultSet);
				
			} catch (SQLException e) {
					log.error("that would be a sql exception thrown."+ e.getClass());// TODO Auto-generated catch block
					e.printStackTrace();
				}
		return null;
	}
	
	public List<EmployeeRequest> getRequestsByStatus(RequestStatus status) {
		
		sqlQuery="SELECT r.REIMBURSEMENT_ID, r.REQUEST_DATE, e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME, r.DESCRIPTION, r.AMOUNT," 
				+" rs.STATUS, r.PROCESSED_DATE, r.MANAGER_ID, e2.FIRST_NAME AS M_FIRST_NAME, e2.LAST_NAME AS M_LAST_NAME"
				+" FROM EMPLOYEE e " 
				+" INNER JOIN REIMBURSEMENT r ON e.EMPLOYEE_ID = r.EMPLOYEE_ID " 
				+" INNER JOIN REQUEST_STATUS rs ON r.STATUS_ID = rs.REQUEST_STATUS_ID" 
				+" LEFT OUTER JOIN EMPLOYEE e2 ON r.MANAGER_ID = e2.EMPLOYEE_ID"
				+" WHERE rs.REQUEST_STATUS_ID = ?";
		
		try(Connection dbConnection = DbConnectionUtil.getHardCodedConnection();
				PreparedStatement queryStatement = dbConnection.prepareStatement(sqlQuery);){
			queryStatement.setInt(1, + status.ordinal());
			
			ResultSet resultSet= queryStatement.executeQuery();
				
				return retrieveRequest(resultSet);
				
			} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		return null;

	}
	
	public List<EmployeeRequest> getRequestsByEmployee(int employeeID) {
		
		List<EmployeeRequest> employeeRequests=new ArrayList<EmployeeRequest>();
		sqlQuery="SELECT r.REIMBURSEMENT_ID, r.REQUEST_DATE, e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME, r.DESCRIPTION, r.AMOUNT," 
				+" rs.STATUS, r.PROCESSED_DATE, r.MANAGER_ID, e2.FIRST_NAME AS M_FIRST_NAME, e2.LAST_NAME AS M_LAST_NAME"
				+" FROM EMPLOYEE e " 
				+" INNER JOIN REIMBURSEMENT r ON e.EMPLOYEE_ID = r.EMPLOYEE_ID " 
				+" INNER JOIN REQUEST_STATUS rs ON r.STATUS_ID = rs.REQUEST_STATUS_ID" 
				+" LEFT OUTER JOIN EMPLOYEE e2 ON r.MANAGER_ID = e2.EMPLOYEE_ID"
				+" WHERE e.EMPLOYEE_ID = ?";
		
		try(Connection dbConnection = DbConnectionUtil.getHardCodedConnection();
				PreparedStatement queryStatement = dbConnection.prepareStatement(sqlQuery);){
			
			queryStatement.setInt(1, employeeID);
			ResultSet resultSet= queryStatement.executeQuery();
				
				employeeRequests= retrieveRequest(resultSet);
			} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		return employeeRequests;
	}
	
public EmployeeRequest getRequestById(int reimbursementRequestID) {
	
		
		List<EmployeeRequest> employeeRequests=new ArrayList<EmployeeRequest>();
		sqlQuery="SELECT r.REIMBURSEMENT_ID, r.REQUEST_DATE, e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME, r.DESCRIPTION, r.AMOUNT," 
				+" rs.STATUS, r.PROCESSED_DATE, r.MANAGER_ID, e2.FIRST_NAME AS M_FIRST_NAME, e2.LAST_NAME AS M_LAST_NAME"
				+" FROM EMPLOYEE e " 
				+" INNER JOIN REIMBURSEMENT r ON e.EMPLOYEE_ID = r.EMPLOYEE_ID " 
				+" INNER JOIN REQUEST_STATUS rs ON r.STATUS_ID = rs.REQUEST_STATUS_ID" 
				+" LEFT OUTER JOIN EMPLOYEE e2 ON r.MANAGER_ID = e2.EMPLOYEE_ID"
				+" WHERE r.REIMBURSEMENT_ID = ?";
		
		try(Connection dbConnection = DbConnectionUtil.getHardCodedConnection();
				PreparedStatement queryStatement = dbConnection.prepareStatement(sqlQuery);){
			
				queryStatement.setInt(1,reimbursementRequestID);
				ResultSet resultSet= queryStatement.executeQuery();
				
				employeeRequests= retrieveRequest(resultSet);
			} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		return employeeRequests.size()>0 ? employeeRequests.get(0) : null;
	}
	
	public EmployeeRequest addNewRequest(ReimbursementRequest request) {
		
		request.setRequestId(getNewRequestId());
		request.setStatus(RequestStatus.pending);
		request.setRequestDate(Date.valueOf(LocalDate.now()));
		
		
		
		sqlQuery="INSERT INTO REIMBURSEMENT (REIMBURSEMENT_ID, DESCRIPTION, AMOUNT, REQUEST_DATE, EMPLOYEE_ID, STATUS_ID) VALUES (?,?,?,?,?,?)";
		try(Connection dbConnection = DbConnectionUtil.getHardCodedConnection();
				PreparedStatement queryStatement = dbConnection.prepareStatement(sqlQuery);){
			queryStatement.setInt(1, request.getRequestId());
			queryStatement.setString(2, request.getDescription());
			queryStatement.setDouble(3, request.getAmount());
			queryStatement.setDate(4, (Date) request.getRequestDate());
			queryStatement.setInt(5, request.getEmployeeId());
			queryStatement.setInt(6, request.getStatus().ordinal());
			int affectedRows=queryStatement.executeUpdate();
			System.out.println(" " +affectedRows);
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return getRequestById(request.getRequestId());

	}
	
	public int getNewRequestId(){
		int newRequestId = 0;
		
		sqlQuery="SELECT  MAX(REIMBURSEMENT_ID) AS MAX_REQUEST_ID FROM REIMBURSEMENT r";
		
						try(Connection dbConnection = DbConnectionUtil.getHardCodedConnection();
				Statement queryStatement = dbConnection.createStatement();){
				ResultSet resultSet= queryStatement.executeQuery(sqlQuery);
				if(resultSet.next()){
					newRequestId=(resultSet.getInt("MAX_REQUEST_ID")+1);
					}
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return newRequestId;
	}
	
	public boolean changeRequestStatus(int requestId, int statusId, int managerId) {
		sqlQuery="UPDATE REIMBURSEMENT SET STATUS_ID=?, MANAGER_ID=?, PROCESSED_DATE=? WHERE REIMBURSEMENT_ID=" + requestId;
		try(Connection dbConnection = DbConnectionUtil.getHardCodedConnection();
				PreparedStatement queryPStatement = dbConnection.prepareStatement(sqlQuery);){
				queryPStatement.setInt(1, statusId);
				queryPStatement.setInt(2, managerId);
				queryPStatement.setDate(3, Date.valueOf(LocalDate.now()));
				queryPStatement.executeUpdate();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	private List<EmployeeRequest>retrieveRequest(ResultSet resultSet) throws SQLException {
		
		requests = new ArrayList<EmployeeRequest>();
		while(resultSet.next()){
			EmployeeRequest request= new EmployeeRequest();
			request.setDescription(resultSet.getString("DESCRIPTION"));
			request.setAmount(resultSet.getDouble("AMOUNT"));
			request.setRequestDate(resultSet.getDate("REQUEST_DATE"));
			request.setEmployeeId(resultSet.getInt("EMPLOYEE_ID"));
			request.setEmplFirstName(resultSet.getString("FIRST_NAME"));
			request.setEmplLastName(resultSet.getString("LAST_NAME"));
			request.setRequestStatus(resultSet.getString("STATUS"));
			request.setManagerId(resultSet.getInt("MANAGER_ID"));
			request.setMgrFirstName(resultSet.getString("M_FIRST_NAME"));
			request.setMgrLastName(resultSet.getString("M_LAST_NAME"));
			request.setRequestId(resultSet.getInt("REIMBURSEMENT_ID"));
			request.setProcessedDate(resultSet.getDate("PROCESSED_DATE"));
			requests.add(request);
			
			
		}
		
		return requests;
	}
	
	private String getCurrentDateTimeAsString()
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
		return dtf.format(getCurrentDateTime());
	}
	
	private LocalDateTime getCurrentDateTime()
	{
		//DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
		return LocalDateTime.now();  
	}

	@Override
	public void deleteRequestById(int reimbursementRequestID) {
		// TODO Auto-generated method stub
		sqlQuery = "DELETE FROM REIMBURSEMENT WHERE REIMBURSEMENT_ID =?";
		
		try(Connection dbConnection = DbConnectionUtil.getHardCodedConnection();
				PreparedStatement queryStatement = dbConnection.prepareStatement(sqlQuery);){
				queryStatement.setInt(1, reimbursementRequestID);
			int resultSet= queryStatement.executeUpdate();
			} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
	}
	
	
	
}
