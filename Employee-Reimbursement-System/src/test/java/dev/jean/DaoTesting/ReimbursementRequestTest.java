package dev.jean.DaoTesting;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import dev.jean.daos.ReimbursementRequestDao;
import dev.jean.daos.ReimbursementRequestDaoImpl;
import dev.jean.models.EmployeeRequest;

public class ReimbursementRequestTest {

	@Test
	public void test() {
		
		ReimbursementRequestDao requestDao = new ReimbursementRequestDaoImpl();
		int employeeId=1;
		
		List<EmployeeRequest> requests=requestDao.getRequestsByEmployee(employeeId);
		assertEquals(6, requests.size());
	}

}
