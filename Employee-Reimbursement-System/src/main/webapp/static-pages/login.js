$( document ).ready(function() {
    //document is ready
	
	//Hide login error alert by default
	$("#alertError").hide();
	
	//Let's add the click event for the button
	
	$( "#btnLogin" ).click(function() {
	  
	  //call the login method
	  login();
	  
	});
});

function login() {
  let userName = $("#email").val();
  let password = $("#pwd").val();
  
  $("#alertError").hide();
  
  if(userName == "")
  {
	  $("#alertError").show();
	  $("#errorDetails").text("Please provide your email");
	  $("#email").focus();
  }
  else if(password == "")
  {
	  $("#alertError").show();
	  $("#errorDetails").text("Please provide your password");
	  $("#email").focus();
  }
  else
  {
	let manager = $("#mgrRadio").is(":checked");
	console.log(manager);
	let credentials = { userName, manager, password };

	const loginUrl = "http://localhost:8080/Employee-Reimbursement-System/login";
	performAjaxPostRequest(
	loginUrl,
	JSON.stringify(credentials),
	handleSuccessfulLogin,
	handleUnsuccessfulLogin
	);
  }
    
  
}

function handleSuccessfulLogin(responseText) {
  console.log("Success! You're logged in");
  console.log(responseText);
  let employee = JSON.parse(responseText);
  console.log(employee.firstName);
  
  sessionStorage.setItem("employee", responseText);
  if(employee.manager)
  {
	$(location).attr('href', 'request-list.html');  
  }
  else
  {
	$(location).attr('href', 'my-requests.html');  
  }
  
  
  /*document.getElementById("error-msg").hidden = true;
  closeModal();
  let token = responseText;
  sessionStorage.setItem("token", token);
  toggleLoginToLogout();
  displayLoggedInUser();*/
}

function handleUnsuccessfulLogin() {
  console.log("Login unsuccessful");
  $("#alertError").show();
  $("#errorDetails").text("Username, role or password is invalid.");
}