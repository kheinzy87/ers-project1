package dev.jean.models;

import java.util.Date;
import dev.jean.enums.RequestStatus;

public class ReimbursementRequest {
	private int requestId;
	private String description;
	private double amount;
	public RequestStatus status;
	private Date requestDate;
	private Date processedDate;
	private int employeeId;
	private int managerId;
	
	
	
	public ReimbursementRequest() {
		super();
			
	}
	
	public ReimbursementRequest(String description, double amount, RequestStatus status, Date requestDate, int employeeId) {
			super();
			this.description=description;
			this.amount=amount;
			this.status=status;
			this.requestDate=requestDate;
			this.employeeId=employeeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public RequestStatus getStatus() {
		return status;
	}

	public void setStatus(RequestStatus status) {
		this.status = status;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getManagerId() {
		return managerId;
	}

	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}
	

}
