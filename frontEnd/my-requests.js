var employee;
const reimbursementRequestUrl = "http://localhost:8080/Employee-Reimbursement-System/requests";
var currentRequestId;

$( document ).ready(function() {
    //document is ready
	
	//Hide login error alert by default
	$("#alertError").hide();
	
	
	//Add click event listener to submit button
	$( "#btnAdd" ).click(function() {
	  
	  //Clear the fields
	  resetFields();
	  
	});
	
	//Add click event listener to submit button
	$( "#btnSubmit" ).click(function() {
	  
	  //submit the employee request
	  submitEmployeeRequest();
	  
	});
	
	let employeeStr = sessionStorage.getItem("employee")
	if(!employeeStr)
	{
		$(location).attr('href', 'login.html');
	}
	else
	{
		employee = JSON.parse(employeeStr);
		$("#navbardrop").text(`${employee.firstName} ${employee.lastName}`);
		getReimbursmentRequests(employee.employeeId);
	}

});



function getReimbursmentRequests(employeeId) {
 	
	let url = `${reimbursementRequestUrl}?employeeId=${employeeId}`;
	//console.log(url); 
	performAjaxGetRequest(url, getEmployeeRequests);
}

function getEmployeeRequests(responseText) {
  
  let requests = JSON.parse(responseText);
  renderEmployeeRequests(requests);
  
}

function renderEmployeeRequests(requests)
{
	console.log(requests);
	let data = "";
	for(i=0; i< requests.length;i++)
	{
		//
		let requestDate = formatDate(requests[i].requestDate);
		let processedDate = (requests[i].processedDate!=null)? formatDate(requests[i].processedDate) : "";
		
		data = data+ "<tr id=tRow"+ requests[i].requestId +">"
					+ 	"<td>"+ requestDate+"</td>"
					+ 	"<td>"+ requests[i].description+"</td>"
					+ 	"<td>"+ requests[i].amount+"</td>"
					+ 	"<td>"+requests[i].requestStatus+"</td>"
					+ 	"<td>"+ processedDate +"</td>"
					+ 	"<td title='Delete'>"
					+ 	"	<button type='button' class='btn btn-danger' onclick='deleteEmployeeRequest("+ requests[i].requestId +")'>"
					+ 	"	  <i class='material-icons align-middle pointer'>delete</i>"
					+ 	"	</button>"
					+	"</td>"
					+"</tr>";
	}
	
	$("#tbodyRequests").html(data);
}

function formatDate(timestamp)
{
	return (new Date(timestamp)).toISOString().split('T')[0];
}


function submitEmployeeRequest()
{
	$("#alertError").hide();
	
	var description = $("#reqDescription").val();
	var amount = parseFloat($("#reqAmount").val());
	console.log(amount);
	
	if(description == "")
	{
	  $("#alertError").show();
	  $("#errorDetails").text("Please provide the description");
	  $("#reqDescription").focus();
	}
	else if(isNaN(amount) || amount < 1 )
	{
	  $("#alertError").show();
	  $("#errorDetails").text("Please provide the amount");
	  $("#reqAmount").focus();
	}
	else
	{
		let employeeId = employee.employeeId;
		let employeeRequest = { description, amount, employeeId };
		
		performAjaxPostRequest(
		reimbursementRequestUrl,
		JSON.stringify(employeeRequest),
		handleSuccessfulRequestSubmission,
		handleUnsuccessfulRequestSubmission
		);
	}
}

function handleSuccessfulRequestSubmission(responseText) {
  
	$('#RequestModal').modal('hide');
	resetFields();

	let newRequest = JSON.parse(responseText);
	let requestDate = formatDate(newRequest.requestDate);
	let processedDate = (newRequest.processedDate!=null)? formatDate(newRequest.processedDate) : "";

	let newRowContent = "<tr id=tRow"+ newRequest.requestId +">"
					+ 	"<td>"+ requestDate+"</td>"
					+ 	"<td>"+ newRequest.description+"</td>"
					+ 	"<td>"+ newRequest.amount+"</td>"
					+ 	"<td>"+ newRequest.requestStatus+"</td>"
					+ 	"<td>"+ processedDate +"</td>"
					+ 	"<td title='Delete'>"
					+ 	"	<button type='button' class='btn btn-danger' onclick='deleteEmployeeRequest("+ newRequest.requestId +")'>"
					+ 	"	  <i class='material-icons align-middle pointer'>delete</i>"
					+ 	"	</button>"
					+	"</td>"
					+"</tr>";
  
  $(newRowContent).appendTo($("#tbodyRequests"));
  
}

function resetFields()
{
	$("#reqDescription").val("");
	$("#reqAmount").val("");
}

function handleUnsuccessfulRequestSubmission() {
  console.log("Login unsuccessful");
  $("#alertError").show();
  $("#errorDetails").text("The request could not be submitted. Please try again later. If the issue persists, contact the administrator");
}

function deleteEmployeeRequest(requestId)
{
	if(confirm("Are you sure you want to delete this reimbursement request?"))
	{
		currentRequestId = requestId;
		let url = `${reimbursementRequestUrl}?requestId=${requestId}`;
				
		performAjaxDeleteRequest(
		url,
		handleDeleteReimbursementRequest,
		
		);
	}
}

function handleDeleteReimbursementRequest(responseText)
{
	$('#tRow'+currentRequestId).remove();
	alert("Your reimbursement request has been deleted successfully.");
}