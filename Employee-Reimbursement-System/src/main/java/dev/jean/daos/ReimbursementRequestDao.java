package dev.jean.daos;

import java.sql.SQLException;
import java.util.List;

import dev.jean.enums.RequestStatus;
import dev.jean.models.EmployeeRequest;
import dev.jean.models.ReimbursementRequest;

public interface ReimbursementRequestDao {

		public List<EmployeeRequest> getAllEmployeeRequests() throws SQLException;
		public List<EmployeeRequest> getRequestsByStatus(RequestStatus status);
		public List<EmployeeRequest> getRequestsByEmployee(int employeeId);
		public EmployeeRequest addNewRequest(ReimbursementRequest request);
		public boolean changeRequestStatus(int requestId, int statusId, int managerId);
		public EmployeeRequest getRequestById(int reimbursementRequestID);
		public void deleteRequestById(int reimbursementRequestID);
}
