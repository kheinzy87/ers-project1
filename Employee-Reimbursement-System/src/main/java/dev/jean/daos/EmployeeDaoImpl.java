package dev.jean.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dev.jean.models.Employee;
import dev.jean.models.EmployeeRequest;
import dev.jean.util.DbConnectionUtil;

public class EmployeeDaoImpl implements EmployeeDao{
	//Employee employee = new Employee();
	String emplQuery;

	@Override
	public Employee getEmployeeByUsername(String userName) {
		Employee employee = null;
		emplQuery="SELECT * FROM EMPLOYEE WHERE EMAIL='" +userName + "'";
		
				try(Connection dbConnection = DbConnectionUtil.getHardCodedConnection();
						Statement queryStatement = dbConnection.createStatement();){
					ResultSet resultSet=queryStatement.executeQuery(emplQuery);
					List<Employee> employees = retrieveEmployee(resultSet);
					
					if(employees.size()> 0)
					{
						employee = employees.get(0);
					}
				}
				catch(Exception e)
				{}
		return employee;
	}
				private List<Employee>retrieveEmployee(ResultSet resultSet) throws SQLException {
					
					List<Employee> employeeList = new ArrayList<Employee>();
					while(resultSet.next()){
						Employee employee= new Employee();
						
						employee.setEmployeeId(resultSet.getInt("EMPLOYEE_ID"));
						employee.setFirstName(resultSet.getString("FIRST_NAME"));
						employee.setLastName(resultSet.getString("LAST_NAME"));
						employee.setEmail(resultSet.getString("EMAIL"));
						employee.setPhoneNumber(resultSet.getString("PHONE_NUMBER"));
						employee.setAddress(resultSet.getString("ADDRESS"));
						employee.setManager(isManager(resultSet.getString("MANAGER")));
						employee.setPassword(resultSet.getString("PASSWORD"));
						
						employeeList.add(employee);
						
					}
					
					return employeeList;
				}
				
				private boolean isManager(String mgr)
				{
					return mgr.equalsIgnoreCase("Y");
				}
}


