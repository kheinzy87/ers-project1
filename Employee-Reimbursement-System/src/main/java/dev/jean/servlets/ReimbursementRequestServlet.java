package dev.jean.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.jean.enums.RequestStatus;
import dev.jean.models.EmployeeRequest;
import dev.jean.models.ReimbursementRequest;
import dev.jean.models.RequestUpdate;
import dev.jean.services.ReimbursementRequestService;



public class ReimbursementRequestServlet extends HttpServlet{
	
	/** Using the log4j to display to the console.
	 * 
	 */
	Logger log =LogManager.getRootLogger();
	private static final long serialVersionUID = 1L;
	private ReimbursementRequestService requestService= new ReimbursementRequestService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		//System.out.println("GET request to reimbursement request Servlet");
		//String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		log.info("GET request to reimbursement request Servlet");
		List<EmployeeRequest> requests = new ArrayList<EmployeeRequest>();
		
		String strEmployeeId = request.getParameter("employeeId");
		String strStatusId = request.getParameter("statusId");
	
				
		if(strEmployeeId != null)
		{
			try {
				requests = requestService.getRequestByEmployee(Integer.parseInt(strEmployeeId));
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(strStatusId != null)
		{
			
			int statusId = Integer.parseInt(strStatusId);
			RequestStatus requestStatus = RequestStatus.values()[statusId];
			
			try {
				requests = requestService.getRequestByStatus(requestStatus);
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			try {
				requests = requestService.getAllRequests();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		ObjectMapper om = new ObjectMapper();
		String requestJson = om.writeValueAsString(requests);
		PrintWriter pw = response.getWriter();
		pw.write(requestJson);
		pw.close();	
		
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
		
		
		String clientHttpRequest = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			
		ObjectMapper objMapper = new ObjectMapper();
		ReimbursementRequest emplRequest = objMapper.readValue(clientHttpRequest, ReimbursementRequest.class);
		
		EmployeeRequest newRequest = requestService.addNewEmployeeRequest(emplRequest);
		
		ObjectMapper om = new ObjectMapper();
		String requestJson = om.writeValueAsString(newRequest);
		PrintWriter pw = response.getWriter();
		pw.write(requestJson);
		pw.close();	
	}
	
	
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		

		String clientHttpRequest = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			
		ObjectMapper objMapper = new ObjectMapper();
		RequestUpdate requestUpdate = objMapper.readValue(clientHttpRequest, RequestUpdate.class);
		
		requestService.updateRequestStatus(requestUpdate.getRequestId(), requestUpdate.getStatusId(), requestUpdate.getManagerId());
	}
	
	
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String strRequestId = request.getParameter("requestId");
		int requestId = Integer.parseInt(strRequestId);
		
		requestService.deleteRequestById(requestId);
	}
	
}
