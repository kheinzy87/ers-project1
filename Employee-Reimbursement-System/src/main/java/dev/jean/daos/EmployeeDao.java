package dev.jean.daos;

import java.util.List;

import dev.jean.models.Employee;
import dev.jean.models.EmployeeRequest;

public interface EmployeeDao {
	public Employee getEmployeeByUsername(String userName);
}
