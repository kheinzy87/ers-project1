package dev.jean.models;

import java.util.Date;

public class EmployeeRequest {
	
	private int requestId;
	private Date requestDate;
	private int employeeId;
	private String emplFirstName;
	private String emplLastName;
	private String description;
	private double Amount;
	private String requestStatus;
	private Date processedDate;
	private int managerId;
	private String mgrFirstName;
	private String mgrLastName;
	
	
	public int getRequestId() {
		return requestId;
	}
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmplFirstName() {
		return emplFirstName;
	}
	public void setEmplFirstName(String emplFirstName) {
		this.emplFirstName = emplFirstName;
	}
	public String getEmplLastName() {
		return emplLastName;
	}
	public void setEmplLastName(String emplLastName) {
		this.emplLastName = emplLastName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return Amount;
	}
	public void setAmount(double amount) {
		Amount = amount;
	}
	public String getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	public Date getProcessedDate() {
		return processedDate;
	}
	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}
	public int getManagerId() {
		return managerId;
	}
	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}
	public String getMgrFirstName() {
		return mgrFirstName;
	}
	public void setMgrFirstName(String mgrFirstName) {
		this.mgrFirstName = mgrFirstName;
	}
	public String getMgrLastName() {
		return mgrLastName;
	}
	public void setMgrLastName(String mgrLastName) {
		this.mgrLastName = mgrLastName;
	}
	
	
}
