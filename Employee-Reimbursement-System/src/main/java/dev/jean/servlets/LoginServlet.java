package dev.jean.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.jean.models.Employee;
import dev.jean.models.Login;
import dev.jean.services.LoginService;
import dev.jean.services.PasswordAuthentication;

public class LoginServlet extends HttpServlet{
	
	Logger log = LogManager.getRootLogger();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		LoginService loginService= new LoginService();
		ObjectMapper objMapper = new ObjectMapper();
		String requestJson = "";
		
		String clientHttpRequest = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		Login login = objMapper.readValue(clientHttpRequest, Login.class);
		
		Employee employee = loginService.getEmployeeByUsername(login.getUserName());
		
		if (employee!=null && (login.isManager() == employee.isManager()) && isPasswordValid(login.getPassword(),employee.getPassword())) {
			
			//return employee
			employee.setPassword(""); // for security reasons password was removed.
			requestJson = objMapper.writeValueAsString(employee);
			PrintWriter pw = response.getWriter();
			pw.write(requestJson);
			pw.close();	
			
		}
		else
		{
			//Error error = new Error(404, "Credentials invalid");
			//requestJson = objMapper.writeValueAsString(error);
			response.sendError(404, "Credentials invalid");
			
			
		}
		
		/*PrintWriter pw = response.getWriter();
		pw.write(requestJson);
		pw.close();	*/
		
	}
	
	private boolean isPasswordValid(String passwordProvided, String passwordSaved)
	{
		//PasswordAuthentication pAuth = new PasswordAuthentication();
		//boolean passwordValid = pAuth.authenticate(login.getPassword().toCharArray(), employee.getPassword());
		
		return passwordProvided.equals(passwordSaved);
	}
	
	
}
