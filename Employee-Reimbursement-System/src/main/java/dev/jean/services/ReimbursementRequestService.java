package dev.jean.services;

import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dev.jean.daos.ReimbursementRequestDao;
import dev.jean.daos.ReimbursementRequestDaoImpl;
import dev.jean.enums.RequestStatus;
import dev.jean.models.EmployeeRequest;
import dev.jean.models.ReimbursementRequest;

public class ReimbursementRequestService {
	
	private static Logger log = LogManager.getRootLogger();
	
	private ReimbursementRequestDao requestDao = new ReimbursementRequestDaoImpl();
	
	
	public List<EmployeeRequest> getAllRequests() throws SQLException{
		log.info("Returning all existing requests from the database");
		
		return requestDao.getAllEmployeeRequests();
		
	}
	
	public List<EmployeeRequest> getRequestByStatus(RequestStatus requestStatus)throws SQLException{
		return requestDao.getRequestsByStatus(requestStatus);
		
	}
	
	public List<EmployeeRequest> getRequestByEmployee(int employeeId)throws SQLException{
		return requestDao.getRequestsByEmployee(employeeId);
		
	}
	
	public EmployeeRequest addNewEmployeeRequest(ReimbursementRequest newRequest) {
		return requestDao.addNewRequest(newRequest);
	}
	
	public boolean updateRequestStatus(int requestId, int statusId, int managerId) {
		return requestDao.changeRequestStatus(requestId, statusId, managerId);
	}
	
	public void deleteRequestById(int requestId) {
		requestDao.deleteRequestById(requestId);
	}
	
	public EmployeeRequest getRequestById(int reimbursementRequestID) {
		return requestDao.getRequestById(reimbursementRequestID);
	}
}
