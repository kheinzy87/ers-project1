package dev.jean.DaoTesting;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import dev.jean.daos.EmployeeDao;
import dev.jean.daos.EmployeeDaoImpl;
import dev.jean.models.Employee;
import dev.jean.models.EmployeeRequest;

public class EmployeeDaoTest {

	@Test
	public void test() {
		
		EmployeeDao employeeDao = new EmployeeDaoImpl();
		String employeeUsername="paul@hotmail.com";
		
		Employee employee=employeeDao.getEmployeeByUsername(employeeUsername);
		assertEquals("paul@hotmail.com", employee.getEmail());
	}
}