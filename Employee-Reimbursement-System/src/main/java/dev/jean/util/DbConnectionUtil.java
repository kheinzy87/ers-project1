package dev.jean.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnectionUtil {
	private static Connection connection;

	public static Connection getHardCodedConnection() throws SQLException {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String url = "jdbc:oracle:thin:@//heinz-db-1109.c7q1h5ii1tar.us-east-2.rds.amazonaws.com:1521/ORCL";
		String username = "admin";
		String password = "MyAWSAccountDatabase2021$";
		if(connection == null || connection.isClosed()) {
			connection = DriverManager.getConnection(url, username, password);
		}
		return connection;
	}
}
