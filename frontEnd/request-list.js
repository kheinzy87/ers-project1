var employee;
const reimbursementRequestUrl = "http://localhost:8080/Employee-Reimbursement-System/requests";
var currentRequestId;

$( document ).ready(function() {
    //document is ready
	/*
	//Hide login error alert by default
	$("#alertError").hide();
	
	
	//Add event listener to submit button
	$( "#btnAdd" ).click(function() {
	  
	  //Clear the fields
	  resetFields();
	  
	});
	*/
	//Add change event listener to statut select option
	$( "#selStatus" ).change(function() {
	  
	  //submit the employee request
	  getReimbursmentRequestsByStatus();
	  
	});
	
	let employeeStr = sessionStorage.getItem("employee")
	if(!employeeStr)
	{
		$(location).attr('href', 'login.html');
	}
	else
	{
		employee = JSON.parse(employeeStr);
		$("#navbardrop").text(`${employee.firstName} ${employee.lastName}`);
		getReimbursmentRequestsByStatus();
	}

});


function getReimbursmentRequestsByStatus() {
 	
	let statusId = $("#selStatus").val();
	
	let url = reimbursementRequestUrl;
	
	if(statusId!="")
	{
		url = `${reimbursementRequestUrl}?statusId=${statusId}`;
	}
	
	performAjaxGetRequest(url, getEmployeeRequests);
}

function getEmployeeRequests(responseText) {
  
  let requests = JSON.parse(responseText);
  renderEmployeeRequests(requests);
  
}

function renderEmployeeRequests(requests)
{
	let data = "";
	for(i=0; i< requests.length;i++)
	{
		//
		let requestDate = formatDate(requests[i].requestDate);
		let processedDate = (requests[i].processedDate!=null)? formatDate(requests[i].processedDate) : "";
		let manager = (requests[i].processedDate!=null)? `${requests[i].mgrFirstName} ${requests[i].mgrLastName}` : "";
		
		let disabled = (requests[i].requestStatus != "PENDING")? "disabled" : "";
		data = data+ "<tr id=tRow"+ requests[i].requestId +">"
					+ 	"<td>"+ requestDate+"</td>"
					+ 	"<td>"+ requests[i].employeeId+"</td>"
					+ 	"<td>"+ `${requests[i].emplFirstName} ${requests[i].emplLastName}`+"</td>"
					+ 	"<td>"+ requests[i].description+"</td>"
					+ 	"<td>"+ requests[i].amount+"</td>"
					+ 	"<td>"+requests[i].requestStatus+"</td>"
					+ 	"<td>"+ processedDate +"</td>"
					+ 	"<td>"+ manager +"</td>"
					+ 	"<td >"
					+ 	"	<button type='button' title='Approve' class='btn btn-primary' "+ disabled +" onclick='updateRequestStatus("+ requests[i].requestId +", 1)'>"
					+ 	"	  <i class='material-icons align-middle pointer'>check_circle</i>"
					+ 	"	</button>"
					+ 	"	<button type='button' title='Deny' class='btn btn-danger' "+ disabled +" onclick='updateRequestStatus("+ requests[i].requestId +", 2)'>"
					+ 	"	  <i class='material-icons align-middle pointer'>cancel</i>"
					+ 	"	</button>"
					+	"</td>"
					+"</tr>";
	}
	
	$("#tbodyRequests").html(data);
}

function formatDate(timestamp)
{
	return (new Date(timestamp)).toISOString().split('T')[0];
}


function updateRequestStatus(requestId, statusId)
{
	let action = (statusId == 1)? "approve" : "deny";
	if(confirm(`Are you sure you want to ${action} this request?`))
	{
		let managerId = employee.employeeId;
		let requestUpdate = {requestId, managerId, statusId};
		performAjaxPutRequest(
		reimbursementRequestUrl,
		JSON.stringify(requestUpdate),
		handleSuccessfulRequestStatusUpdate,
		handleUnsuccessfulRequestStatusUpdate
		);
	}
}

function handleSuccessfulRequestStatusUpdate(responseText) {
	/*
	$('#RequestModal').modal('hide');
	resetFields();

	let newRequest = JSON.parse(responseText);
	let requestDate = formatDate(newRequest.requestDate);
	let processedDate = (newRequest.processedDate!=null)? formatDate(newRequest.processedDate) : "";

	let newRowContent = "<tr id=tRow"+ newRequest.requestId +">"
					+ 	"<td>"+ requestDate+"</td>"
					+ 	"<td>"+ newRequest.description+"</td>"
					+ 	"<td>"+ newRequest.amount+"</td>"
					+ 	"<td>"+ newRequest.requestStatus+"</td>"
					+ 	"<td>"+ processedDate +"</td>"
					+ 	"<td title='Delete'>"
					+ 	"	<button type='button' class='btn btn-danger' onclick='deleteEmployeeRequest("+ newRequest.requestId +")'>"
					+ 	"	  <i class='material-icons align-middle pointer'>delete</i>"
					+ 	"	</button>"
					+	"</td>"
					+"</tr>";
  
  $(newRowContent).appendTo($("#tbodyRequests"));
  */
  getReimbursmentRequestsByStatus();
  
}

function resetFields()
{
	$("#reqDescription").val("");
	$("#reqAmount").val("");
}

function handleUnsuccessfulRequestStatusUpdate() {
  
  alert("The status of the request could not be updated. Please try again later. If the issue persists, contact the administrator");
}

function deleteEmployeeRequest(requestId)
{
	if(confirm("Are you sure you want to delete this reimbursement request?"))
	{
		currentRequestId = requestId;
		let url = `${reimbursementRequestUrl}?requestId=${requestId}`;
				
		performAjaxDeleteRequest(
		url,
		handleDeleteReimbursementRequest,
		
		);
	}
}