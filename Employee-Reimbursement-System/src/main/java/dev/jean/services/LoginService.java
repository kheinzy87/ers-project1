package dev.jean.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dev.jean.daos.EmployeeDao;
import dev.jean.daos.EmployeeDaoImpl;
import dev.jean.models.Employee;

public class LoginService {
	Logger log= LogManager.getRootLogger();
	EmployeeDao employeeDao = new EmployeeDaoImpl();
	
	public Employee getEmployeeByUsername(String userName) {
		log.info("Getting email address as the username for employee");
		
		return employeeDao.getEmployeeByUsername(userName);
	}

}
