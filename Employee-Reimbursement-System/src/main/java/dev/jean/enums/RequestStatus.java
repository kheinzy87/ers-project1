package dev.jean.enums;

public enum RequestStatus {
	pending,
	approved,
	denied
}
